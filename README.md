Ctags generator for Bluespec
============================

Generate tags that can be used with Ctags for Bluespec. Run the following commands
in the root of your Bluespec Source Directory.

```bash
    chmod a+x parse.py
    ./parse.py .
```

This should create the tags file in your root directory. Open Vim from the
root directory and when you open Bluespec file, you can jump to the tag 
under the cursor using `Ctrl+]`. `<Ctrl-w>]` opens the definition in a new window. `Ctrl+t` takes you back to your source code.

Current Status
==============
* [x] typedef struct
* [x] typedef enum
* [ ] typedef alias
* [ ] interfaces
* [ ] sub-interfaces
* [ ] methods
* [ ] functions
* [ ] modules
